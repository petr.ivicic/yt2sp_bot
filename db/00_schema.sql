-- SETTINGS
ALTER DATABASE postgres
SET TIMEZONE TO "UTC";

-- TABLES
CREATE TABLE "spotify_tracks" (
   track_id VARCHAR PRIMARY KEY,
   api_data JSONB NOT NULL
);

CREATE TABLE "youtube_tracks" (
   v_hash VARCHAR PRIMARY KEY
);

CREATE TABLE "track_share" (
    id SERIAL PRIMARY KEY,
    author BIGINT NOT NULL,
    youtube VARCHAR references youtube_tracks(v_hash) NOT NULL,
    spotify VARCHAR references spotify_tracks(track_id),
    posted TIMESTAMP DEFAULT current_timestamp
);
