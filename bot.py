from typing import List
import re
import logging
import sys
from os import environ as env
from typing import Union
import json

from src.db import get_conn, close_conn, run_sql

import spotipy
from spotipy.oauth2 import SpotifyClientCredentials

import discord
from discord.ext import commands

from youtube_title_parse import get_artist_title

from yt_dlp import YoutubeDL

log_handler = logging.StreamHandler(sys.stdout)
log = logging.getLogger('discord')
log.setLevel(logging.DEBUG)
log.addHandler(log_handler)

def map_over(obj, functions: List[callable], options = dict()):
    temp = obj
    for f in functions:
        temp = f(temp, **options)
    return temp

# expects arg to curry to be the last of positional
def curry(function, *args, **kwargs):
    return lambda arg: function(*args, arg, **kwargs)

def extract_video_url(message):
    # Match the video ID in the full YouTube URL or a shortened URL
    pattern = r'(youtube\.com\/watch\?v=|youtu\.be\/|youtube\.com\/embed\/)([A-Za-z0-9_-]+)'
    match = re.search(pattern, message)

    if match:
        video_url = ''.join(match.groups())
        log.info(f"Found youtube video url {video_url} in \"{message}\"")
        return video_url, match.groups()[1]
    else:
        return None

def get_yt_artist_and_song(url):
    title = ""
    channel_name = ""
    ytdl_opts = {}
    with YoutubeDL(ytdl_opts) as ydl:
        info_dict = ydl.extract_info(url, download=False)
        title = info_dict.get('title', None)
        channel_name = info_dict.get('channel', None)
    if (not title) or (not channel_name):
        raise Exception("Title couldn't be parsed")
    channel_name = map_over(channel_name, [
        curry(re.sub, r"\s*-\s*Topic", ''),
        curry(re.sub, r"\([^)]*\)", ''),
        curry(re.sub, r"\[[^)]*\]", '')
    ]).strip()

    #author = response['items'][0]['snippet']['author']
    #duration = response['items'][0]['contentDetails']['duration']
    #thumbnail_url = response['items'][0]['snippet']['thumbnails']['default']['url']
    title = map_over(title, [
        curry(re.sub, r"\([^)]*\)", ''),
        curry(re.sub, r"\[[^)]*\]", '')
    ]).strip()

    if parsed_artist_title := get_artist_title(title):
        method = "title artist"
        use_artist, use_title = parsed_artist_title
    else:
        method = "channel artist"
        use_artist, use_title = channel_name, title

    log.info(f"Video \"{url}\" title: \"{use_title}\" artist: \"{use_artist}\" using method: \"{method}\"")
    return use_artist, use_title

spotify_cid = env["SPOTIFY_CID"]
spotify_secret = env["SPOTIFY_SECRET"]

#Authentication - without user
client_credentials_manager = SpotifyClientCredentials(client_id=spotify_cid, client_secret=spotify_secret)
sp = spotipy.Spotify(client_credentials_manager = client_credentials_manager)

def lookup_spotify_track(artist, name, manual_query=None):
    query = manual_query or f"artist:{artist} track:{name}"
    log.info(f"Running spotify query \"{query}\"")
    #search_dict = sp.search(urllib.parse.quote(query), limit=1) or {}
    search_dict = sp.search(query, limit=1, type="track") or {}
    found_tracks = search_dict["tracks"]["items"]
    if (not search_dict) or (not found_tracks):
        log.info("No track found for query \"{query}\"")
        return None
    track = found_tracks[0]
    artists = set(artist["name"].lower() for artist in track["artists"])
    track_id = track['id']
    track_link = track["external_urls"]["spotify"] if len(found_tracks) > 0 else ""
    spotify_track_name = map_over(track["name"], [
        curry(re.sub, r"\s*-.+remaster", '', flags=re.IGNORECASE)
    ]).strip()
    similarity_with_query = len({artist.lower(), name.lower()}.intersection(artists | {spotify_track_name.lower()})) == 2
    if not similarity_with_query:
        similarity_explanation = f"{artist.lower()}:{name.lower()} vs {artists}:{spotify_track_name.lower()}"
        log.info(f"Result for query \"{query}\" is link \"{track_link}\". Not similar enough to query: {similarity_explanation}")
        return None

    log.info(f"Result for query \"{query}\" is link \"{track_link}\"")
    return track_link, search_dict, track_id
        
spotify_cache = dict()
def spotify_track(artist: str, name: str):
    for options in [(artist, name), (artist, name, f"{artist} {name}")]:
        found = lookup_spotify_track(*options)
        if found:
            track_link = found[0]
            api_dict = found[1]
            track_id = found[2]
            return track_link, api_dict, track_id
    return None


def extract_spotify_track_url(message):
    # Match the track ID in the full Spotify URL
    pattern = r'(https\:\/\/open\.spotify\.com\/track\/)([A-Za-z0-9_-]+)'

    if match := re.search(pattern, message):
        video_url = ''.join(match.groups())
        log.info(f"Found spotify track url {video_url} in \"{message}\"")
        return match.groups()[1]
    return None


def get_sp_artist_and_song(track_id):
    # Extract artist and track names from spotify API
    api_dict = sp.track(track_id)
    use_artist = api_dict['artists'][0]['name']
    use_title = api_dict['name']
    url = api_dict['external_urls']['spotify']
    log.info(f"Track \"{url}\" title: \"{use_title}\" artist: \"{use_artist}\"")
    return use_artist, use_title, api_dict


def youtube_video(artist: str, track: str):
    # Search YouTube for video with artist and track names in title
    search_term = artist + " - " + track
    with YoutubeDL({'format': 'bestaudio', 'noplaylist': 'True'}) as ydl:
        result = ydl.extract_info(f"ytsearch:{search_term}", download=False)
        return result['entries'][0]['webpage_url'], result['entries'][0]['id']


listen_to_this = int(env["MUSIC_SHARE_CHANNEL"])
bot_controls = int(env["BOT_CONTROLS_CHANNEL"])

intents = discord.Intents.default()
intents.message_content = True
bot = commands.Bot(command_prefix='/', intents=intents)

class Database():
    def __init__(self, conn=None):
        self.conn = conn or get_conn() 

    def lookup_youtube_to_spotify_link(self, v_hash:str) -> Union[str, None]:
        query = "SELECT st.track_id, st.api_data \
                 FROM spotify_tracks st \
                 JOIN track_share ts ON st.track_id = ts.spotify \
                 JOIN youtube_tracks yt ON ts.youtube = yt.v_hash \
                 WHERE yt.v_hash = %s \
        "
        params = (v_hash,)
        ret = run_sql(self.conn.cursor(), query, params)
        link = None
        if len(ret) > 0:
            api_json = ret[0][1]
            found_tracks = api_json["tracks"]["items"]
            track = found_tracks[0]
            link = track["external_urls"]["spotify"]
        return link

    def is_present_yt(self, video_id:str):
        params = (video_id,)
        query = "SELECT EXISTS(SELECT 1 FROM youtube_tracks WHERE v_hash = %s);"
        return run_sql(self.conn.cursor(), query, params)[0][0]

    def is_present_sp(self, track_id:str):
        params = (track_id,)
        query = "SELECT EXISTS(SELECT 1 FROM spotify_tracks WHERE track_id = %s);"
        return run_sql(self.conn.cursor(), query, params)[0][0]

    def save_sharing(self, author: str, vhash: str = None, sp_tid: str = None, sp_data: dict = None):
        success = bool(sp_tid) and bool(sp_data)
        already_there_yt = self.is_present_yt(vhash)
        already_there_sp = self.is_present_sp(sp_tid)
        queries = [
            (not already_there_yt, "INSERT INTO youtube_tracks (v_hash) VALUES (%s); COMMIT", (vhash,)),
            (not already_there_sp and success, "INSERT INTO spotify_tracks (track_id, api_data) VALUES (%s, %s); COMMIT", (sp_tid, json.dumps(sp_data))),
        ]
        for pred, query, params in queries:
            if pred:
                run_sql(self.conn.cursor(), query, params)

        query = "INSERT INTO track_share (author, youtube, spotify) VALUES (%s, %s, %s)"
        params = (author, vhash, sp_tid)
        run_sql(self.conn.cursor(), query, params)
        
database = Database()
monitor = {listen_to_this, bot_controls}

@bot.event
async def on_message(message):
    if message.author.bot or message.channel.id not in monitor:
        return
    spotify_link = ""
    options = {"reference": message}
    log.info(f"Processing message: {message.content}")
    extracted = extract_video_url(message.content)
    if extracted:
        video_url = extracted[0]
        video_id = extracted[1]
        if spotify_link := database.lookup_youtube_to_spotify_link(video_id):
            log.info(f"Resolution from database {video_url} -> {spotify_link}")
            await message.channel.send(spotify_link, **options)
            return

        artist, song = get_yt_artist_and_song(video_url)
        if data := spotify_track(artist, song):
            spotify_link = data[0]
            data_dict = data[1]
            track_id = data[2]
            database.save_sharing(message.author.id, video_id, track_id, data_dict)
            await message.channel.send(spotify_link, **options)
        return
    if spotify_song_id := extract_spotify_track_url(message.content):
        song_artist, song_title, api_dict = get_sp_artist_and_song(spotify_song_id)
        if yt_data := youtube_video(song_artist, song_title):
            youtube_link, video_id = yt_data
            database.save_sharing(message.author.id, video_id, spotify_song_id, api_dict)
            await message.channel.send(youtube_link, **options)

token = env["DISCORD_BOT_TOKEN"]
log.info("Starting bot")
bot.run(token, log_handler=log_handler)
